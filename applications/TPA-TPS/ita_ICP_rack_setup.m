function ms = ita_addCalibratedICP_rack_setup(ms,ch)
%automatically set calibration values for all 32 Channels on the ICP rack
% Inputs:
%   ms  needs to be a measurement setup with only the Channels of the ICP
%       Rack
%   ch: Channel idx of the channels to add

