% RWTH - ITA Toolbox
% Version 2.0 (BSD License Ready) 1-Jun-2011
% Files
% 
% toolbox\applications\Analytic\Impedance_Calculator
% 
%   Impedanz                                <ITA-Toolbox>
% 
% toolbox\applications\Analytic\Impedance_Calculator\Material_Classes
% 
% 
% toolbox\applications\Analytic\Impedance_Calculator
% 
%   get_muend_corr                          <ITA-Toolbox>
% 
% toolbox\applications\Analytic\Monopole
% 
% 
% toolbox\applications\Binaural-HRTF\CrosstalkCancellation
% 
%   smooth_balasz                           <ITA-Toolbox>
% 
% toolbox\applications\Binaural-HRTF\CrosstalkCancellation\thirdParty
% 
%   parfilt                                 filtering by parallel second-order sections.
%   parfiltid                               System identification in the form of second-order parallel filters for a given
% 
% toolbox\applications\Binaural-HRTF\HRTF_class
% 
%   ita_itaHRTF_examples                    <ITA-Toolbox>
%   test_rbo_DTF_itaHRTF                    'azRing' -> only HRTFs on the ring of el = 90�
%   test_rbo_itaAntro_adaption              <ITA-Toolbox>
%   test_rbo_pressureSphere                 <ITA-Toolbox>
% 
% toolbox\applications\Binaural-HRTF\HRTFarc
% 
% 
% toolbox\applications\Binaural-HRTF\HpTF_class
% 
%   ita_headphone_equalization              <ITA-Toolbox>
% 
% toolbox\applications\Binaural-HRTF
% 
% 
% toolbox\applications\BulkyAudioData
% 
% 
% toolbox\applications\HTMLhelp
% 
% 
% toolbox\applications\Hardware\AirflowResistance
% 
% 
% toolbox\applications\Hardware\Arduino
% 
% 
% toolbox\applications\Hardware\Audiometer
% 
%   ita_audiometer_preferences              <ITA-Toolbox>
%   ita_audiometer_redoFreq                 <ITA-Toolbox>
% 
% toolbox\applications\Hardware\FrontendControl\FrontendControl_Callbacks
% 
%   ita_menucallback_AurelioControl         <ITA-Toolbox>
%   ita_menucallback_ModulITAControl        <ITA-Toolbox>
%   ita_menucallback_RoboControl            <ITA-Toolbox>
% 
% toolbox\applications\Hardware\FrontendControl\additionalRoutines
% 
% 
% toolbox\applications\Hardware\FrontendControl
% 
%   ita_nexus_checkCommand                  checks if input string is a valid serial-command for B&K Nexus devices
%   ita_robocontrolcenter_gui               M-file for robocontrolcenter.fig
% 
% toolbox\applications\Hardware\ITAMotorControl\ClassStuff\itaMotorControl
% 
% 
% toolbox\applications\Hardware\ITAMotorControl
% 
%   ita_micArray_sort                       -
%   ita_movtec_init                         this function looks for a defined com-port in
%   ita_movtec_xytable_measurement          ITA_MOVTEC_XY-TABLE_MEASUREMENTis a set of functions to use the
%   ita_movtec_xytable_move                 this function move the table either in x- or in
%   ita_movtec_xytable_reference_move       ITA_MOVTEC_XY-TABLE_REFERENCE_MOVEthis function moves the table in to
%   ita_movtec_xytable_wait                 ITA_MOVTEC_XY-TABLE_WAITthis function should be used to wait for
% 
% toolbox\applications\Hardware\ITAdevices
% 
% 
% toolbox\applications\Hardware\MicroflownImpedanceProcessor
% 
%   ita_get_surface_impedance               calculates the surface impedance for given raw Data measured with the ITA Microflown PU-Mini Probe
% 
% toolbox\applications\Hardware\Microflown
% 
%   ita_microflown_1_prepare                <ITA-Toolbox>
%   ita_microflown_2_latency                <ITA-Toolbox>
%   ita_microflown_3_freefield              <ITA-Toolbox>
%   ita_microflown_4_run                    <ITA-Toolbox>
% 
% toolbox\applications\Hardware\Scattering
% 
%   ita_ISO_limits_scattering               returns ISO17497-1 limits
%   ita_diffusion_coefficient               diffusion coefficient according to AES-4id-2001
%   ita_scattering_coefficient_diffuse      random-incidence scattering coefficient
%   ita_scattering_coefficient_freefield    calculate free-field scattering coefficient
%   motor_move                              <ITA-Toolbox>
%   motor_setup                             <ITA-Toolbox>
%   scattering_box_abs_coordinates          <ITA-Toolbox>
% 
% toolbox\applications\Hardware\Tracking\Optitrack\NatNetSDK\NatNet_SDK_2.10\NatNetSDK\Samples\MatlabWrapper\natnet
% 
% 
% toolbox\applications\Hardware\Tracking\Optitrack\NatNetSDK\NatNet_SDK_2.10\NatNetSDK\Samples\Matlab
% 
% 
% toolbox\applications\Hardware\Tracking\Optitrack
% 
% 
% toolbox\applications\Hardware\Tracking\Polhemus
% 
% 
% toolbox\applications\Hardware\Vibrometer
% 
%   ita_measurement_laser                   Run a measurement with the laser-vibrometer
%   ita_preferences_vibrometer              <ITA-Toolbox>
%   ita_vibro_convertViv                    convert a vivo-file to cell array of vibometer commands
%   ita_vibro_findGoodSpot                  emulates the microsteps of newer laser-vibrometers
%   ita_vibro_lasergui                      GUI to control the Polytec laser-vibrometer
%   ita_vibro_manual_mesh                   <ITA-Toolbox>
%   ita_vibro_sendCommand                   send a command via RS232 to the polytec laser-vibrometer
%   ita_vibro_triangulation                 converts mesh coordinates into commands for the polytec laser-vibrometer
%   ita_vibro_vivo                          converts mesh coordinates into commands for the polytec laser-vibrometer
% 
% toolbox\applications\Hardware\ZoomH6
% 
% 
% toolbox\applications\Hardware\osc\externalPackages
% 
% 
% toolbox\applications\Hardware\osc
% 
% 
% toolbox\applications\Laboratory\MedAk1
% 
% 
% toolbox\applications\Laboratory\MedAk2
% 
% 
% toolbox\applications\Laboratory\Tools
% 
% 
% toolbox\applications\Laboratory\V0
% 
% 
% toolbox\applications\Laboratory\V10
% 
% 
% toolbox\applications\Laboratory\V11
% 
% 
% toolbox\applications\Laboratory\V1
% 
%   ita_menucallback_Bandfiltern            <ITA-Toolbox>
%   ita_menucallback_EDC                    <ITA-Toolbox>
%   ita_menucallback_Kalibrierung           <ITA-Toolbox>
%   ita_menucallback_KompletteMessung       <ITA-Toolbox>
%   ita_menucallback_MesssetupEditieren     <ITA-Toolbox>
%   ita_menucallback_MessungSchallleistung  <ITA-Toolbox>
%   ita_menucallback_MessungStarten         <ITA-Toolbox>
%   ita_menucallback_OktavenUndSchalleistung<ITA-Toolbox>
%   ita_menucallback_T30                    <ITA-Toolbox>
% 
% toolbox\applications\Laboratory\V2\ext\export_fig
% 
% 
% toolbox\applications\Laboratory\V2\ext\findobj
% 
% 
% toolbox\applications\Laboratory\V2
% 
%   ita_v2laboratory_gui_abai               <ITA-Toolbox>
% 
% toolbox\applications\Laboratory\V5
% 
%   ita_menucallback_OrtskurveFestgebremst  <ITA-Toolbox>
%   ita_menucallback_OrtskurveHoheResonanz  <ITA-Toolbox>
%   ita_menucallback_OrtskurveMitGehaeuseGedaempft<ITA-Toolbox>
%   ita_menucallback_OrtskurveMitGehaeuseUngedaempft<ITA-Toolbox>
%   ita_menucallback_OrtskurveOhneGehaeuse  <ITA-Toolbox>
%   ita_menucallback_Ortskurvenschreiber    <ITA-Toolbox>
% 
% toolbox\applications\Laboratory\V6
% 
% 
% toolbox\applications\Laboratory\V7
% 
% 
% toolbox\applications\Laboratory
% 
% 
% toolbox\applications\ListeningTests\AuditorySelectiveAttention
% 
%   checkTracker                            <ITA-Toolbox>
%   ita_asa_main                            Dies ist die main-funktion. Von hier aus werden die anderen Funktionen zum
%   reaktionsZeit                           <ITA-Toolbox>
% 
% toolbox\applications\ListeningTests\Database
% 
% 
% toolbox\applications\ListeningTests\Relative MAA
% 
% 
% toolbox\applications\ListeningTests
% 
%   ita_listeningtest_AB_cmp_generate_input <ITA-Toolbox>
%   ita_listeningtest_JND                   <ITA-Toolbox>
% 
% toolbox\applications\ListeningTests\javaGUI
% 
%   createJavaBlockArrays                   <ITA-Toolbox>
% 
% toolbox\applications\ListeningTests
% 
% 
% toolbox\applications\Measurement\AbsorptionCmeasRevChamber
% 
%   ita_generate_protocol_for_revChamber_absMeas<ITA-Toolbox>
% 
% toolbox\applications\Measurement\ClassStuff
% 
%   ita_device_list_handle                  <ITA-Toolbox>
%   test_ita_MeasurementSetup               <ITA-Toolbox>
% 
% toolbox\applications\Measurement\ImpedanceTube\Protocol
% 
%   ita_preferences_protocol                ITA_PREFERENCES_Protocol -
% 
% toolbox\applications\Measurement\ImpedanceTube
% 
%   ita_kundt_class2Protocol                <ITA-Toolbox>
%   ita_kundt_postprocessing_gui            <ITA-Toolbox>
%   ita_menucallback_KundtsTube             <ITA-Toolbox>
% 
% toolbox\applications\Measurement\LoudspeakerTools\Menucallbacks
% 
%   ita_menucallback_CombineNearfieldAndFarfieldMeasurements<ITA-Toolbox>
%   ita_menucallback_FreefieldResponse      <ITA-Toolbox>
%   ita_menucallback_THD                    <ITA-Toolbox>
%   ita_menucallback_ThieleSmallParameters  <ITA-Toolbox>
% 
% toolbox\applications\Measurement\LoudspeakerTools
% 
%   ita_add_nearfield_farfield_measurements for loudspeaker near-field measurements
%   ita_loudspeakertools_distortions        Calculate the THD, THD-N and HD's with
%   ita_thiele_small                        Calculation of Thiele-Small Paramters
% 
% toolbox\applications\Measurement\MLS
% 
% 
% toolbox\applications\Measurement\MeasurementCallbacks
% 
%   ita_ChooseMeasurement_gui               <ITA-Toolbox>
%   ita_guisupport_measurement_get_global_MS<ITA-Toolbox>
%   ita_menucallback_EditMeasurement        <ITA-Toolbox>
%   ita_menucallback_MeasuringStationPreferences<ITA-Toolbox>
%   ita_menucallback_NewMeasurementSetup    <ITA-Toolbox>
%   ita_menucallback_NewMeasuringStation    <ITA-Toolbox>
%   ita_menucallback_RunMeasurement2File    <ITA-Toolbox>
% 
% toolbox\applications\Measurement\NI_DAQ
% 
% 
% toolbox\applications\Measurement
% 
%   ita_sound_level_meter                   Real-time calibrated sound level meter
% 
% toolbox\applications\NAH
% 
% 
% toolbox\applications\Psychoacoustics
% 
%   test_ita_loudness                       <ITA-Toolbox>
%   test_ita_loudness_timevariant           <ITA-Toolbox>
%   test_ita_sharpness                      <ITA-Toolbox>
% 
% toolbox\applications\RoomAcoustics\AdditionalRoutines
% 
% 
% toolbox\applications\RoomAcoustics\BuildingAcoustics
% 
%   ita_soundInsulationIndexAirborne        sound insulation acc. to ISO 717-1
%   ita_soundInsulationIndexImpact          ita_soundInsulationIndexAirbornesound insulation acc. to ISO 717-2 or ASTM E989
% 
% toolbox\applications\RoomAcoustics\RoomacousticsCallbacks
% 
%   ita_menucallback_EnergyDecayCurve_SchroederBackwardsIntegration<ITA-Toolbox>
%   ita_menucallback_EnergyParameters       <ITA-Toolbox>
%   ita_menucallback_IREndDetect            <ITA-Toolbox>
%   ita_menucallback_ISO3382_Measurement    <ITA-Toolbox>
%   ita_menucallback_InterauralCrossCorrelation<ITA-Toolbox>
%   ita_menucallback_LateralFraction        <ITA-Toolbox>
%   ita_menucallback_ReverberationChamberAbsorptionCoefficient<ITA-Toolbox>
%   ita_menucallback_ReverberationTime      <ITA-Toolbox>
%   ita_menucallback_RoomAcoustic           <ITA-Toolbox>
%   ita_menucallback_RoomAcousticDefaultParameters<ITA-Toolbox>
%   ita_menucallback_SoundPower             <ITA-Toolbox>
% 
% toolbox\applications\RoomAcoustics
% 
%   ita_preferences_roomacoustics           <ITA-Toolbox>
%   ita_roomacoustics_EDC                   calculate reverse-time integrated impulse response
% 
% toolbox\applications\SignalProcessing\Beamforming
% 
%   gauss_seidel                            solve linear system of equations using Gauss-Seidel method in matrix form
%   ita_beam_steeringVector                 <ITA-Toolbox>
%   ita_preferences_beamforming             <ITA-Toolbox>
%   test_ita_beamforming                    <ITA-Toolbox>
% 
% toolbox\applications\SignalProcessing\MonopoleDecomposition
% 
% 
% toolbox\applications\SignalProcessing\Nonlinear\guicallbacks
% 
%   ita_menucallback_PolynomialSeries       <ITA-Toolbox>
% 
% toolbox\applications\SignalProcessing\Nonlinear
% 
% 
% toolbox\applications\SignalProcessing\Nonlinear\toBeTested
% 
% 
% toolbox\applications\SignalProcessing\PoleZeroProny
% 
%   ita_AudioAnalyticRationalMatrix2itaAudioMatrix-
%   ita_audio2zpk_rationalfit               ITA_AUDIO2ZPKPole-Zero-Analysis
%   ita_result2audio_pdi                    Pole-Zero Interpolation Method
%   pdi_prony                               Prony's method for time-domain IIR filter design (memory fix!).
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\ClassStuff
% 
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification
% 
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\SoundFieldIndicators\CEF2
% 
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\SoundFieldIndicators\ClassStuff
% 
%   ita_analytic_directivity_freefield      <ITA-Toolbox>
%   ita_analytic_directivity_sample_function<ITA-Toolbox>
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\SoundFieldIndicators\SoundFieldModels
% 
%   ita_sfm_incoherent_noise                <ITA-Toolbox>
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\SoundFieldIndicators\Verification
% 
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification\SoundFieldIndicators
% 
%   ita_analytic_coeherence_estimate_function<ITA-Toolbox>
%   ita_hearingaids_calculate_acoustic_center<ITA-Toolbox>
%   ita_hearingaids_calculate_diffuse_compensation<ITA-Toolbox>
%   ita_hearingaids_compensate_displacement <ITA-Toolbox>
%   ita_sfa_plot_mediaDB_results            <ITA-Toolbox>
%   ita_sfi_calculate_acoustic_center       <ITA-Toolbox>
%   ita_sfi_coherence                       <ITA-Toolbox>
%   ita_sfi_compensate_displacement         <ITA-Toolbox>
%   ita_sfi_sfdspace_figure                 <ITA-Toolbox>
%   ita_sfi_sfispace_figure                 <ITA-Toolbox>
%   ita_sfis_calculate_diffuse_compensation <ITA-Toolbox>
% 
% toolbox\applications\SignalProcessing\SoundFieldClassification
% 
%   ita_cef_parse_results                   <ITA-Toolbox>
%   ita_cef_plotmaker                       <ITA-Toolbox>
%   ita_preferences_sfc                     <ITA-Toolbox>
%   ita_sfi_sfdspace_figure                 <ITA-Toolbox>
%   savethis                                <ITA-Toolbox>
%   test_asa                                <ITA-Toolbox>
%   test_sfc_cef                            <ITA-Toolbox>
% 
% toolbox\applications\SoundFieldSimulation\Comsol\Demos
% 
%   ita_comsol_demo                         <ITA-Toolbox>
%   ita_comsol_demo_binaural_receiver       <ITA-Toolbox>
%   ita_comsol_demo_boundary_conditions     <ITA-Toolbox>
%   ita_comsol_demo_close                   <ITA-Toolbox>
%   ita_comsol_demo_init                    <ITA-Toolbox>
%   ita_comsol_demo_simulation              <ITA-Toolbox>
%   ita_comsol_demo_sources                 <ITA-Toolbox>
%   ita_comsol_demo_visualizer              <ITA-Toolbox>
% 
% toolbox\applications\SoundFieldSimulation\Comsol\Import
% 
% 
% toolbox\applications\SoundFieldSimulation\Diffraction
% 
% 
% toolbox\applications\SoundFieldSimulation\Diffraction\auralization
% 
% 
% toolbox\applications\SoundFieldSimulation\Diffraction
% 
% 
% toolbox\applications\SoundFieldSimulation\FluidFEM
% 
%   get_object                              <ITA-Toolbox>
%   get_property_group                      <ITA-Toolbox>
%   vol_hex_gauss                           Gives mass- (M) and compressibility matrix (S) back by using a
%   vol_hex_gauss_lin                       Gives mass- (M) and compressibility matrix (S) back by using a
%   vol_tri_gauss                           Gives mass- (M) and compressibility matrix (S) back by using a
%   vol_tri_gauss_lin                       Gives mass- (M) and compressibility matrix (S) back by using a
%   writeLogFile                            <ITA-Toolbox>
% 
% toolbox\applications\SoundFieldSimulation\ImageSources\DirectionalShoebox
% 
% 
% toolbox\applications\SoundFieldSimulation\ImageSources
% 
% 
% toolbox\applications\SoundFieldSimulation\InputData
% 
% 
% toolbox\applications\SoundFieldSimulation\Raven\AC3D
% 
% 
% toolbox\applications\SoundFieldSimulation\Raven
% 
%   determineAirAbsorptionParameter         <ITA-Toolbox>
%   readRavenMaterial                       <ITA-Toolbox>
% 
% toolbox\applications\SoundFieldSimulation\WRW
% 
% 
% toolbox\applications\SpatialAudio\BalloonClass
% 
%   ita_read_itaBalloon                     reads an itaBalloon out of it's mat-file and actualizes it's
% 
% toolbox\applications\SpatialAudio\SourceTranslation\m
% 
% 
% toolbox\applications\SpatialAudio
% 
%   ita_binauralMixdown                     ITA_3DA_BINAURALMIXDOWN Produces a 2-Channel binaural stream out of
%   ita_hoa_a2bFormat                       Converts an A-Format recording to B-Format
% 
% toolbox\applications\SphericalHarmonics\fromEnrique
% 
%   rafaely_plane                           <ITA-Toolbox>
%   sph_DOA                                 <ITA-Toolbox>
%   sph_harmonics                           <ITA-Toolbox>
%   steering_matrix                         <ITA-Toolbox>
% 
% toolbox\applications\SphericalHarmonics
% 
%   ita_sph_besselh_diff                    <ITA-Toolbox>
%   ita_sph_besselj_diff                    <ITA-Toolbox>
%   ita_sph_bessely_diff                    <ITA-Toolbox>
%   ita_sph_degreeorder2linear              linear index for SH-coefficients
%   ita_sph_distance                        <ITA-Toolbox>
%   ita_sph_linear2degreeorder              degree/order index for SH-coefficients
%   ita_sph_matrix2vector                   <ITA-Toolbox>
%   ita_sph_northpolecapSH                  SH-coefficients for vibrating cap on north pole
%   ita_sph_order_energy                    <ITA-Toolbox>
%   ita_sph_plot_coefs                      <ITA-Toolbox>
% 
% toolbox\applications\SphericalHarmonics\ita_sph_sampling
% 
%   ita_sph_sampling                        <ITA-Toolbox>
%   ita_sph_sampling_GfAI_array             <ITA-Toolbox>
%   ita_sph_sampling_GfAI_array_xml         <ITA-Toolbox>
%   ita_sph_sampling_V000H000               <ITA-Toolbox>
%   ita_sph_sampling_equiangular            <ITA-Toolbox>
%   ita_sph_sampling_mic32berlin            <ITA-Toolbox>
%   ita_sph_sampling_reorder                <ITA-Toolbox>
% 
% toolbox\applications\SphericalHarmonics
% 
%   ita_sph_vector2matrix                   <ITA-Toolbox>
%   ita_sph_wignerD                         Wigner-D matrix
%   ita_sph_wignerD_real                    Wigner-D matrix
%   ita_sph_xcorr                           spatial correlation in SH-domain
%   test_itaSuperSph                        <ITA-Toolbox>
% 
% toolbox\applications\TPA-TPS
% 
%   ita_otpa_lohmann                        /-----------------------------------------------------------------------\
%   ita_plate_analytic_FRF                  <ITA-Toolbox>
%   ita_plate_mobilities                    CHO_PLATE_MOBILITIES calculates the out-of-plane point and transfer
%   ita_plot_rectangular                    <ITA-Toolbox>
%   ita_tpa_DOF2index                       <ITA-Toolbox>
%   ita_tps_DOF2index                       <ITA-Toolbox>
%   ita_tps_two_port_transformation         include two-port into Ys
% 
% toolbox\applications\VirtualAcoustics\VA
% 
%   itaVA_example_text_to_speech            itaVA simple example code for a text-to-speech sound source
%   itaVA_example_tracked_hato_listener     itaVA tracked head-above-torso listener example code
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.5
% 
%   daffv15_create_dataset                  OpenDAFFA free, open-source software package for directional audio data,
%   daffv15_effective_filter_bounds         OpenDAFFA free, open-source software package for directional audio data,
%   daffv15_fpad16                          DAFF_FILE_PAD16 Zero-pads a file to a length that is a multiple of 16 Bytes
%   daffv15_lwrmul                          OpenDAFFA free, open-source software package for directional audio data,
%   daffv15_uprmul                          OpenDAFFA free, open-source software package for directional audio data,
%   daffv15_write                           OpenDAFFA free, open-source software package for directional audio data,
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.7
% 
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.7\datafuncs
% 
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.7\hrtfs\ITA
% 
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.7\hrtfs\TH_Koeln_AudioGroup
% 
% 
% toolbox\applications\VirtualAcoustics\openDAFF\OpenDAFFv1.7\hrtfs\TUB
% 
% 
% toolbox\debug
% 
%   test_ita_Value                          <ITA-Toolbox>
%   test_ita_analysis                       <ITA-Toolbox>
%   test_ita_arithmetic                     <ITA-Toolbox>
%   test_ita_class                          <ITA-Toolbox>
%   test_ita_convolve                       <ITA-Toolbox>
%   test_ita_coordinates                    <ITA-Toolbox>
%   test_ita_edit                           <ITA-Toolbox>
%   test_ita_fileIO                         <ITA-Toolbox>
%   test_ita_filter                         <ITA-Toolbox>
%   test_ita_parametric_GUI                 <ITA-Toolbox>
%   test_ita_parse_arguments                <ITA-Toolbox>
%   test_ita_portaudio                      <ITA-Toolbox>
%   test_ita_rms                            <ITA-Toolbox>
% 
% toolbox
% 
% 
% toolbox\kernel\ClassStuff\Meshing
% 
% 
% toolbox\kernel\ClassStuff
% 
%   display_line4commands                   <ITA-Toolbox>
% 
% toolbox\kernel\DSP\Analysis
% 
%   ita_normxcorr2                          calculates the normalized 2D cross-correlation
% 
% toolbox\kernel\DSP\Arithmetic
% 
%   ita_invert_spk                          Invert your spk-data (1 ./ spk)
% 
% toolbox\kernel\DSP\Edit
% 
%   ita_amplify_to                          Amplify itaAudio to specified dB-level (RMS over whole signal)
%   ita_liriFR                              Creates frequency response (FR) of a Linkwitz-Riley filter as itaAudioStruct
%   ita_xcorr_dat                           Calculates the cross-correlation in time-domain
% 
% toolbox\kernel\DSP\Filter
% 
%   ita_filter_peak                         Peak/Bell-Filter / Parametric-EQ
% 
% toolbox\kernel\DSP\Transformation
% 
%   ita_wiener_hopf_factorization           Do Wiener-Hopf Factorization
% 
% toolbox\kernel\DSP
% 
% 
% toolbox\kernel\DataAudio_IO
% 
%   ita_read                                This function reads supported files into the ITA-Toolbox class.
% 
% toolbox\kernel\DataAudio_IO\ita_read
% 
%   ita_read_ita                            <ITA-Toolbox>
%   ita_read_unv                            returns the data of unv-files
%   ita_read_wav                            ITA_WAVREADRead Microsoft WAVE, WAV-EX and Ambisonics
% 
% toolbox\kernel\DataAudio_IO
% 
%   ita_readunvgroups                       read groups from unv-files
%   ita_readunvresults                      Read unv-resultfile written with SoundSolve FE-Solver
%   ita_wavread                             Read Microsoft WAVE, WAV-EX and Ambisonics
% 
% toolbox\kernel\DataAudio_IO\ita_write
% 
%   ita_write_unv                           writes data into unv-files
% 
% toolbox\kernel\DataAudio_IO
% 
%   ita_write_txt                           Write audioObj to txt-File
% 
% toolbox\kernel\Gui\Support
% 
% 
% toolbox\kernel\Gui
% 
%   ita_guimenuentries                      ITA_GUISUPPORT_MENULISTCreate menu-entries
%   ita_plot_gui                            Part of the ITA-Toolbox GUI
%   ita_toolbox_gui                         <ITA-Toolbox>
%   ita_write_gui                           <ITA-Toolbox>
% 
% toolbox\kernel\MetaInfo
% 
% 
% toolbox\kernel\PlayrecMidi
% 
%   ita_midi_menuStr                        <ITA-Toolbox>
%   ita_playrec                             -
%   ita_playrec_show_strings                <ITA-Toolbox>
%   ita_portaudio                           Manages sound in- and output
%   ita_portmidi_menuStr                    <ITA-Toolbox>
%   ita_preferences_playrecmidi             <ITA-Toolbox>
% 
% toolbox\kernel\PlotRoutines\Plottools
% 
%   ita_colormap                            enhancement of the artemis scale, increasing lightness with increasing value -> ideal for color and b&W plots
%   ita_plottools_change_font_in_eps        Change font in an EPS-File
%   ita_plottools_ita_logo                  <ITA-Toolbox>
% 
% toolbox\kernel\PlotRoutines\colormaps
% 
% 
% toolbox\kernel\PlotRoutines
% 
%   ita_pcolor                              <ITA-Toolbox>
%   ita_pcolor_dB                           <ITA-Toolbox>
%   ita_plot_2D                             plot two-dimensional data
%   ita_plot_all                            multi-plot of the input object
%   ita_plot_ccx                            multi-plot of the input object
%   ita_plot_dat                            <ITA-Toolbox>
%   ita_plot_dat_dB                         <ITA-Toolbox>
%   ita_plot_spk                            <ITA-Toolbox>
%   ita_plot_spkgdelay                      <ITA-Toolbox>
%   ita_plot_spkphase                       <ITA-Toolbox>
%   ita_set_plot_preferences                <ITA-Toolbox>
% 
% toolbox\kernel\StandardRoutines
% 
%   finfo                                   <ITA-Toolbox>
%   isincellstr                             This function compares two cell-arrays of strings
%   ita_check4toolboxsetup                  Call Toolbox Setup if out-of-date
%   ita_generateSampling_equiangular        <ITA-Toolbox>
%   ita_mapDataToMesh                       maps two-dimensional data onto a given mesh
%   ita_newm                                Open m-File with ITA Template
%   ita_questiondlg                         <ITA-Toolbox>
%   ita_restore_matlab_default_plot_preferences<ITA-Toolbox>
%   ita_toolbox_version_number              return the number of the current version of the ITA-Toolbox
%   ita_verbose_info                        Warning/ Informing Function of ITA-Toolbox
%   popup_index_to_string                   <ITA-Toolbox>
%   popup_string_to_index                   <ITA-Toolbox>
% 
% toolbox\kernel
% 
%   ita_delete_toolboxpaths                 delete all ITA-Toolbox paths in MATLAB
%   ita_path_handling                       handle the ITA-toolbox paths in MATLAB
% 
% toolbox\tutorials
% 
%   ita_tutorial                            Getting startet with the ITA-Toolbox
%   ita_tutorial_itaHRTF                    <ITA-Toolbox>
