************************************************************************
*******                    ITA-Toolbox for MATLAB              *********
************************************************************************

Please be aware that by using this toolbox you accept the license 
agreement supplied in the 'license.txt' file.
Please report bugs to toolbox-dev@akustik.rwth-aachen.de
MATLAB commands (to be entered in the 'command window') will be denoted in <>. 
The actual command has to be entered without <>.

************************************************************************
** TUTORIAL OVERVIEW **
************************************************************************
The command <ita_tutorialOverview> gives an overview of all the tutorials that are part of the ITA Toolbox.
Each tutorial will be shortly explained below. They are listed in the following: 
1)  ita_tutorial
2)  ita_tutorial_imagesources
3)  ita_tutorial_itaCoordinates
4)  ita_tutorial_itaHRTF
5)  ita_tutorial_record
6)  ita_tutorial_roomacoustics
7)  ita_tutorial_roomacoustics_nonlinear
8)  ita_tutorial_spherical_harmonics
9)  ita_tutorial_transferfunction

************************************************************************

1)  ita_tutorial
    This tutroial is recommended for everyone using the ITA toolbox. 
    The class objects as itaAudio or itaValue are presented as well as some basic signal processing tasks are shown.

2)  ita_tutorial_imagesources
    Here, it is shown how ti create a 3D image source model with the toolbox.

3)  ita_tutorial_itaCoordinates
    This tutorial demonstrates how to use the itaCoodinates class and how to plot spatial/spherical audio data.

4)  ita_tutorial_itaHRTF
    In this tutorial it is shown how to create and work with an HRTF in MATLAB. Especially various plots are introduced.

5)  ita_tutorial_record
    In this tutorial you learn how to set-up the ITA Toolbox for a simple recording task.

6)  ita_tutorial_roomacoustics
    Here, it is shown how to create a room impulse response (RIR). Various room acoustic parameters are introduced and plotted. 
    The command <help ita_roomacoustics> provides an overview of alle room acoustic parameters that can be calculated. 

7)  ita_tutorial_roomacoustics_nonlinear
    This tutorial deals with nonlinear room impulse responses (RIR). They are created, compared to linear ones and various parameters are calculated.

8)  ita_tutorial_spherical_harmonics
    This tutorial demonstrates some of the spherical harmonic signal processing techniques implemented in the ITA Toolbox.

9)  ita_tutorial_transferfunction
    In this tutorial you learn how to measure a transfer function with the ITA Toolbox.

************************************************************************
** Have fun trying the tutorials out! **
************************************************************************
Your ITA-Toolbox Developer Team
